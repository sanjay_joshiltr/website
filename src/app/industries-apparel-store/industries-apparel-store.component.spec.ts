import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndustriesApparelStoreComponent } from './industries-apparel-store.component';

describe('IndustriesApparelStoreComponent', () => {
  let component: IndustriesApparelStoreComponent;
  let fixture: ComponentFixture<IndustriesApparelStoreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndustriesApparelStoreComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IndustriesApparelStoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
