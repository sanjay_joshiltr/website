import { Component, OnInit } from '@angular/core';
import { EmailService } from '../email.service';
import * as nodemailer from 'nodemailer';
import { NgForm, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { SmtpOptions } from 'nodemailer-smtp-transport';
import * as smtpTransport from 'nodemailer-smtp-transport';
import * as SendGrid from "@sendgrid/mail";

declare let $: any;

declare let SendMail : any;

declare function validateForm() : any;

declare function validateName() : any;

declare function validatePhone() : any;

declare function validateEmail() : any;

@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.css']
})
export class ContactusComponent implements OnInit {
  firstname!: string;
  email_id!: string;
  contact!: string;
  message!: string;
  subject =  "Test mail subject";

  postId: any;

  actionUrl = 'https://docs.google.com/forms/u/0/d/e/1FAIpQLSeiV72SLgdCrg_lXzJmW_z6gKw0kf3hxltUaIppHa8SZITxuQ/formResponse';
  
  constructor(private http: HttpClient) {
    
   }

  ngOnInit(): void {
    
    
    

  }

  
  

validateName() {
      
  if(this.firstname?.length == 0) {
    alert("Name can't be blank") ;
    return false;
  }

  if (!this.firstname?.match(/^[a-zA-Z]{3,}(?: [a-zA-Z]+){0,2}$/)) {
    alert("Please enter your correct name") ;//Validation Message
    return false;
  }
  return true;
}


validatePhone() {
      
  if(this.contact?.length == 0) {
    alert("Phone number can't be blank") ;//Validation Message
    return false;
  }

  /*if(!this.contact_number?.match(/^[0]?[789]\d{9}$/)) {
    alert("Please enter a correct phone number") ;//Validation Message
    return false;
  }*/

 return true;

}

validateEmail () {
    
  if(this.email_id?.length == 0) {
    alert("Email can't be blank") ;//Validation Message
    return false;

  }

  if(!this.email_id?.match(/^[A-Za-z\._\-[0-9]*[@][A-Za-z]*[\.][a-z]{2,4}$/)) {
    alert("Please enter a correct email address") ;//Validation Message
    return false;

  }

  return true;

}


validmessage() {
  var msg =$("#Message").val();

    if(this.message?.length == 0 || msg == "") {
      alert("Please enter Message") ;
      return false;
  
    }
    
    return true;
}


validateForm() : any {
  
  let data = new FormData();

  if ( !this.validateName() || !this.validatePhone() || !this.validateEmail() || !this.validmessage() ) {

    alert("Form not submitted");//Validation Message
    return false;
  }else{
    data.append("entry.580637106", this.firstname);
    data.append("entry.15042053", this.contact);
    data.append("entry.1041809111", this.email_id);
    data.append("entry.971859245", this.message);
    
    console.log(this.firstname);
    console.log(this.contact);
    console.log(this.email_id);
    console.log(this.message);
  }

  

  

  //Add success message
  this.http.post(this.actionUrl, data).subscribe((res)=>{
    console.log(res, "Success response");
  }, (errorRes)=>{
    console.log(errorRes, "Error Response");
    if(errorRes.error.statusText == "Unknown Error" || errorRes.error.status === 0){
      alert("Form submitted successfully");
    }
  });
  
}



}
