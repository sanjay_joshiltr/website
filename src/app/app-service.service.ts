import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AppServiceService {

  constructor(private http : HttpClient) { }

  sent(){
    //return this.http.get('/api/sent');
    return this.http.get('/sent');
  }
}
