import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-pricing',
  templateUrl: './pricing.component.html',
  styleUrls: ['./pricing.component.css']
})
export class PricingComponent implements OnInit {
  fullname!: string;
  contact_number!: string;
  email_id!: string;
  org_name!: string;
  message!: string;

  actionUrl = 'https://docs.google.com/forms/u/0/d/e/1FAIpQLSd--mm5YAaMYp1qC9jSZKzQFBGnfwU2VUkAr3i6oPqnzSSlUA/formResponse';

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
  }

  validateName() {
      
    if(this.fullname?.length == 0) {
      alert("Name can't be blank") ;
      return false;
    }

    if (!this.fullname?.match(/^[a-zA-Z]{3,}(?: [a-zA-Z]+){0,2}$/)) {
      alert("Please enter your correct name") ;//Validation Message
      return false;
    }
    return true;
  }
  
  
  validatePhone() {
      
    if(this.contact_number?.length == 0) {
      alert("Phone number can't be blank") ;//Validation Message
      return false;
    }
  
    /*if(!this.contact_number?.match(/^[0]?[789]\d{9}$/)) {
      alert("Please enter a correct phone number") ;//Validation Message
      return false;
    }*/
  
   return true;
  
  }
  
  validateEmail () {
    
    if(this.email_id?.length == 0) {
      alert("Email can't be blank") ;//Validation Message
      return false;
  
    }
  
    if(!this.email_id?.match(/^[A-Za-z\._\-[0-9]*[@][A-Za-z]*[\.][a-z]{2,4}$/)) {
      alert("Please enter a correct email address") ;//Validation Message
      return false;
  
    }
  
    return true;
  
  }
  
  
  validOrg() {
    var orgName =$("#organisation-name").val();

    if(this.org_name?.length == 0 || orgName == "") {
      alert("Please enter organisation-name") ;
      return false;
  
    }
    
    return true;
  }

  
  

  validateForm() : any {
    
    let data = new FormData();
    
    if ( !this.validateName() || !this.validatePhone() || !this.validateEmail() || !this.validOrg() ) {

      alert("Form not submitted");//Validation Message
      return false;
    }else{

      data.append("entry.1871128446", this.fullname);
      data.append("entry.308828301", this.contact_number);
      data.append("entry.600949575", this.email_id);
      data.append("entry.858516406", this.org_name);
      data.append("entry.725154871", this.message);
      
      console.log(this.fullname);
      console.log(this.contact_number);
      console.log(this.email_id);
      console.log(this.org_name);
      console.log(this.message);

    }

    
  
    
  
    //Add success message
    this.http.post(this.actionUrl, data).subscribe((res)=>{
      console.log(res, "Success response");
    }, (errorRes)=>{
      console.log(errorRes, "Error Response");
      if(errorRes.error.statusText == "Unknown Error" || errorRes.error.status === 0){
        alert("Form submitted successfully");
      }
    });
    
  }

}
