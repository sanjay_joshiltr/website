import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import * as AOS from 'aos';
import { Aos } from 'aos';

@Component({
  selector: 'app-whyfrontlyne',
  templateUrl: './whyfrontlyne.component.html',
  styleUrls: ['./whyfrontlyne.component.css'],
  animations: [
    trigger('flyInOut', [
      state('in', style({ opacity:1,transform: 'translateY(0)' })),
      transition('void => *', [
        style({ opacity:0,transform: 'translateY(100%)' }),
        animate(1000)
      ]),
      transition('* => void', [
        animate(200, style({ opacity:0,transform: 'translateY(100%)' }))
      ])
    ]),
    trigger('flyIn', [
      state('in', style({ opacity:1,transform: 'translateY(0)' })),
      transition('void => *', [
        style({ opacity:0,transform: 'translateY(100%)' }),
        animate(3000)
      ]),
      transition('* => void', [
        animate(200, style({ opacity:0,transform: 'translateY(100%)' }))
      ])
    ]),
    trigger('fadeIn', [
      state('in', style({ opacity:1,transform: 'translateY(0)' })),
      transition('void => *', [
        style({ opacity:0,transform: 'translateY(100%)' }),
        animate(1000)
      ]),
      transition('* => void', [
        animate(200, style({ opacity:0,transform: 'translateY(100%)' }))
      ])
    ]),
    trigger('zoom', [
      state('leftTop', style({ opacity:1,transform: 'scale(0.5)' })),
      transition('void => *', [
        style({ opacity:0,transform: 'scale(0.5)', transformOrigin: 'left top' }),
        animate(3000)
      ]),
      transition('* => void', [
        animate(200, style({ opacity:0,transform: 'scale(0.5)' }))
      ])
    ])
  ]
})
export class WhyfrontlyneComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    AOS.init();
  }

}
