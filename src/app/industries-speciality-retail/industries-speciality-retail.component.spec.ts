import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndustriesSpecialityRetailComponent } from './industries-speciality-retail.component';

describe('IndustriesSpecialityRetailComponent', () => {
  let component: IndustriesSpecialityRetailComponent;
  let fixture: ComponentFixture<IndustriesSpecialityRetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndustriesSpecialityRetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IndustriesSpecialityRetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
