import { Component, OnInit } from '@angular/core';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { HttpClient } from '@angular/common/http';

import * as $ from 'jquery';

interface IfcMainObject {
  [key : string]: string;
 }

@Component({
  selector: 'app-schedule-a-demo',
  templateUrl: './schedule-a-demo.component.html',
  styleUrls: ['./schedule-a-demo.component.css']
})
export class ScheduleADemoComponent implements OnInit {
  
  checkboxObj: IfcMainObject = {'cp': ''}

  radioObj: IfcMainObject = {'cp': ''}
  
  cp!: string;
  lms!: string;
  both!: string;
  date!: string;
  timer_1!: string;
  timer_2!: string;
  timer_3!: string;
  fullname!: string;
  email_id!: string;
  contact_number!: string;
  
  actionUrl2 = 'https://docs.google.com/forms/u/0/d/e/1FAIpQLSelLtYQFlYUCWnmBoWfQbw0oWqd2t1de0eeTwB9Cwupbqstcg/formResponse';



  model1!: string;
  
  constructor(private http: HttpClient) { 

    
  }

  ngOnInit(): void {
  }

  validateForm() : any{

    let data = new FormData();

    //data.append("entry.2144212549", this.cp);
    //data.append("entry.2144212549", this.lms);
    //data.append("entry.2144212549", this.both);
    for(let o in this.checkboxObj) { 
      console.log(o);
      
      
      if(this.checkboxObj[o]){
        data.append("entry.2144212549", this.checkboxObj[o]);
      }
    }

    for(let o in this.radioObj) { 
      console.log(o);
      
      
      if(this.radioObj[o]){
        data.append("entry.876542959", this.radioObj[o]);
      }
    }

    data.append("entry.450472989_day", this.date);

    //data.append("entry.876542959", this.timer_1);
    //data.append("entry.876542959", this.timer_2);
    //data.append("entry.876542959", this.timer_3);

    data.append("entry.1989669201", this.fullname);
    data.append("entry.1187725587", this.email_id);
    data.append("entry.1834607629", this.contact_number);
    
    console.log(this.cp);
    console.log(this.lms);
    console.log(this.both);
    console.log(this.date);
    console.log(this.timer_1);
    console.log(this.timer_2);
    console.log(this.timer_3);
    console.log(this.fullname);
    console.log(this.email_id);
    console.log(this.contact_number);
    

    //Add success message
    this.http.post(this.actionUrl2, data).subscribe((res)=>{
      console.log(res, "Success response");
    }, (errorRes)=>{
      console.log(errorRes, "Error Response");
      if(errorRes.error.statusText == "Unknown Error" || errorRes.error.status === 0){
        alert("Form submitted successfully");
      }
    });

    
    

  }

  handelEvent(e : any){
    console.log(e);
    
    this.checkboxObj[e.target.id] = (e.target.checked) ? e.target.value : null
  }

  timeEvent(e : any){
    console.log(e);
    
    this.radioObj[e.target.id] = (e.target.checked) ? e.target.value : null
  }

}
