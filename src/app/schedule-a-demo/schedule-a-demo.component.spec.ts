import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleADemoComponent } from './schedule-a-demo.component';

describe('ScheduleADemoComponent', () => {
  let component: ScheduleADemoComponent;
  let fixture: ComponentFixture<ScheduleADemoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScheduleADemoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScheduleADemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
