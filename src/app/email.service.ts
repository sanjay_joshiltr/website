import { Injectable } from '@angular/core';
import {HttpClient,HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EmailService {

  _url = 'http://localhost:5000/enroll' ;
  constructor(private _http:HttpClient) { }

  sendMessage(body: any){
    return this._http.post<any>(this._url, body);
  }
}
