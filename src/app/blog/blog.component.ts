import { trigger, state, transition, style, animate } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import * as AOS from 'aos';
import { Aos } from 'aos';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css'],
  animations: [
    trigger('fade', [
      transition('void => *', [
        animate(5000)
      ])
    ])
  ]
})
export class BlogComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    AOS.init();
  }

}
