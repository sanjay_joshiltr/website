import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndustriesSupermarketsComponent } from './industries-supermarkets.component';

describe('IndustriesSupermarketsComponent', () => {
  let component: IndustriesSupermarketsComponent;
  let fixture: ComponentFixture<IndustriesSupermarketsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndustriesSupermarketsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IndustriesSupermarketsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
