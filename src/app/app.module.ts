import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, Component } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BsNavbarComponent } from './bs-navbar/bs-navbar.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import { MatNativeDateModule } from '@angular/material/core';



import { HomeComponent } from './home/home.component';
import { WhyfrontlyneComponent } from './whyfrontlyne/whyfrontlyne.component';
import { FeaturesComponent } from './features/features.component';
import { KnowledgeComponent } from './knowledge/knowledge.component';
import { PricingComponent } from './pricing/pricing.component';
import { BsFooterComponent } from './bs-footer/bs-footer.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { SocialmediaiconsComponent } from './socialmediaicons/socialmediaicons.component';
import { TopnavComponent } from './topnav/topnav.component';
import { Sf1Component } from './sf1/sf1.component';
import { Sf2Component } from './sf2/sf2.component';
import { Sf3Component } from './sf3/sf3.component';
import { Sf4Component } from './sf4/sf4.component';
import { Sf5Component } from './sf5/sf5.component';
import { Sf6Component } from './sf6/sf6.component';
import { ContactusComponent } from './contactus/contactus.component';
import { animate } from '@angular/animations';
import { Aos } from 'aos';
import { BlogComponent } from './blog/blog.component';
import { TalktousComponent } from './talktous/talktous.component';
import { ScheduleADemoComponent } from './schedule-a-demo/schedule-a-demo.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { IndustriesComponent } from './industries/industries.component';
import { IndustriesSpecialityRetailComponent } from './industries-speciality-retail/industries-speciality-retail.component';
import { IndustriesSupermarketsComponent } from './industries-supermarkets/industries-supermarkets.component';
import { IndustriesApparelStoreComponent } from './industries-apparel-store/industries-apparel-store.component';



@NgModule({
  declarations: [
    AppComponent,
    BsNavbarComponent,
    HomeComponent,
    WhyfrontlyneComponent,
    FeaturesComponent,
    KnowledgeComponent,
    PricingComponent,
    BsFooterComponent,
    AboutusComponent,
    SocialmediaiconsComponent,
    TopnavComponent,
    Sf1Component,
    Sf2Component,
    Sf3Component,
    Sf4Component,
    Sf5Component,
    Sf6Component,
    ContactusComponent,
    BlogComponent,
    TalktousComponent,
    ScheduleADemoComponent,
    IndustriesComponent,
    IndustriesSpecialityRetailComponent,
    IndustriesSupermarketsComponent,
    IndustriesApparelStoreComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    CarouselModule,
    BrowserAnimationsModule,
    MatDatepickerModule,
    MatInputModule,
    MatNativeDateModule,

    RouterModule.forRoot([
      { path: '', component: HomeComponent },
      { path: 'whyfrontlyne', component: WhyfrontlyneComponent },
      { path: 'features', component: FeaturesComponent },
      { path: 'knowledge', component: KnowledgeComponent },
      { path: 'pricing', component: PricingComponent },
      { path: 'aboutus', component: AboutusComponent},
      { path: 'sf1', component: Sf1Component},
      { path: 'sf2', component: Sf2Component},
      { path: 'sf3', component: Sf3Component},
      { path: 'sf4', component: Sf4Component},
      { path: 'sf5', component: Sf5Component},
      { path: 'sf6', component: Sf6Component},
      { path: 'contactus', component: ContactusComponent},
      { path: 'topnav', component: TopnavComponent},
      { path: 'talktous', component: TalktousComponent},
      { path: 'blog', component: BlogComponent},
      { path: 'schedule_a_demo', component: ScheduleADemoComponent},
      { path: 'industries', component: IndustriesComponent},
      { path: 'industries_speciality_retail', component: IndustriesSpecialityRetailComponent},
      { path: 'industries_supermarkets', component: IndustriesSupermarketsComponent},
      { path: 'industries_store', component: IndustriesApparelStoreComponent}
      

    ]),
     NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
