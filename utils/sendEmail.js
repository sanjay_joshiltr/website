require('dotenv').config();
const sgMail = require('@sendgrid/mail');
//const { text } = require('body-parser');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

const sendEmail=(to, from, subject, text) =>{
    const msg = {
        to,
        from,
        subject,
        html: text
    }   
    sgMail.send(msg, function(err, result){
        if(err){
            console.log("Email Not Sent");
        }else{
            console.log("Email Sent");
        }
    });
};

module.exports = sendEmail;